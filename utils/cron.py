from typing import Callable, List
from functools import wraps
from asyncio import get_running_loop
from utils import import_object
from logging import getLogger

log = getLogger(__name__)


def cron_job(every: int) -> Callable[[Callable], Callable]:
    """
    Make a function able to run periodically
    **Note:** this sends the task to a running `EventLoop`

    Example:
    ```python
    loop = create_event_loop()

    @cron_job(5) # Every 5 seconds
    def hello():
        print('Hello world')

    async def main():
        hello()

    loop.create_task(main())
    loop.run_forever()
    ```
    """

    def decorator(fx: Callable) -> Callable:
        @wraps(fx)
        def wrapper(*args):
            loop = get_running_loop()
            loop.call_later(every, wrapper, *args)
            return fx(*args)

        return wrapper

    return decorator


def load_crons(crons: List[str]):
    """Load and run a list of cron jobs"""
    loop = get_running_loop()

    for cron in crons:
        fx = import_object(cron)

        log.info('Initializing cron "%s"', cron)
        loop.call_soon(fx)
