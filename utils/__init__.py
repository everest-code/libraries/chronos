from typing import Any
import re


def full_qualified(o: Any) -> str:
    """Return the full qualified name of a object (class, function)"""
    klass = o.__class__
    module = klass.__module__

    if module == "builtins":
        return klass.__qualname__  # avoid outputs like 'builtins.str'

    return module + "." + klass.__qualname__


def import_object(source: str) -> Any:
    """Import dinamically an object."""
    regex = re.compile(r"(?P<mod>.+)\.(?P<fx>\w+)")
    data = regex.match(source).groupdict()

    assert data is not None, "source has no correct format"

    mod = __import__(data["mod"])
    return getattr(mod, data["fx"])
