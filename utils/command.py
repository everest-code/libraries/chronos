from typing import Callable, Awaitable, List
from discord import ApplicationContext
from discord.ext.commands import Bot
from utils import import_object
from logging import getLogger

log = getLogger(__name__)


def add_command(
    bot: Bot, command: Callable[[ApplicationContext, ...], Awaitable], **kwargs
):
    """
    Handle procedure to add a command into a discord bot.

    Example:
    ```python
    bot = discord.Bot()

    async def ping(ctx):
            '''Ping to the bot, send a PONG'''
            await ctx.send('PONG')

    add_command(bot, ping)
    ```
    """

    name = command.__name__
    description = command.__doc__.strip().split("\n")[0] if command.__doc__ else None

    bot.slash_command(name=name, description=description, **kwargs)(command)


def load_commands(bot: Bot, commands: List[str]):
    """Load a list of commands into a bot"""
    for command in commands:
        log.info('Loading command "%s"', command)
        fx_command = import_object(command)
        add_command(bot, fx_command)
