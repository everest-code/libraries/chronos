#!/usr/bin/env python
# -*- charset: utf-8 -*-
# -*- author: SGT911 -*-
from dotenv import load_dotenv
from os import environ as env
from logging import basicConfig, getLogger

from discord import Status, Activity, ActivityType
from discord.ext.commands import Bot

from utils.command import load_commands
from utils.cron import load_crons

load_dotenv()
basicConfig(
    level=env.get("LOG_LEVEL", "INFO"),
    format="[%(asctime)s | %(levelname)s][%(name)s %(process)d]: %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S %Z",
)

bot = Bot()
log = getLogger(__name__)


@bot.event
async def on_connect():
    load_commands(bot, env.get("COMMANDS").split(":"))

    log.info("Uploading command list")
    await bot.sync_commands(force=True)

    load_crons(env.get("CRONS").split(":"))


@bot.event
async def on_ready():
    """Handle event when the discord Bot is connected."""
    await bot.change_presence(
        status=Status.idle,
        activity=Activity(
            type=ActivityType.watching,
            name="for your meetings and calendar",
        ),
    )

    log.info("Bot started as: %s", bot.user)


if __name__ == "__main__":
    bot.run(env.get("TOKEN"))
