from discord import ApplicationContext, Option
from logging import getLogger

log = getLogger(__name__)


async def ping(
    ctx: ApplicationContext,
    msg: Option(str, "Text", required=False) = "PONG",
):
    """
    Ping the bot (Dev command)

    Internal function to check if the bot is connected to the **discord WebSocket**
    """

    log.info(
        "Ping triggered by user '%s' on guild '%s' on channel '%s'",
        ctx.user,
        ctx.guild,
        ctx.channel,
    )
    await ctx.respond(msg)
