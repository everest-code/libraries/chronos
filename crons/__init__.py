from utils.cron import cron_job
from logging import getLogger
from os import environ as env

log = getLogger(__name__)


@cron_job(int(env.get("CRON_EVERY", 5)))
def hello():
    log.info("Hello world")
